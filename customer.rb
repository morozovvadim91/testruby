class Customer

	def initialize(full_name city country age salary)
		@full_name = full_name
		@city = city
		@country = country
		@age = age
		@salary = salary
	end
end