require_relative "./spec_helper"
require_relative "../seven_divided"

describe SevenDivided do
	it "Divide on 7 without rest" do 
		expect(SevenDivided.divided_by_7(49)).to eq(true)
	end
	it "Divide on 7 with rest" do
		expect(SevenDivided.divided_by_7(8)).to eq(false)
	end
end