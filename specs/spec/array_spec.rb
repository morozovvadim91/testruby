require_relative "./spec_helper"
require_relative "../array"

describe Array do

	describe "#has_string?" do
		it "String in array" do
			expect([1, 4, "Ruby"].has_string?).to eq(true)
		end
		it "String is not in array" do
			expect([2, 5, 7].has_string?).to eq(false)
		end
	end

	describe "$average" do
		it "return average of array sum" do
			array = [1, 3, 5]
			expect(array.average).to eq((1 + 3 + 5) / array.size)
		end

		it "rise error 'NoElementsInArray' if no array size" do
			array = []
			expect(array.average).to eq(0)
		end

		it "rise error 'NoNumeric' if no array size" do
			array = ["a", "b"]
			expect{ array.average }.to raise_error(TypeError)
		end
	end
end