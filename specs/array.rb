class Array
	def has_string?
		self.any? { |e| e.is_a? String  }
	end

	def average
		self.inject(0) do |sum, el|
			raise TypeError.new("NoNumeric") if self.any? { |el| !(el.is_a? Numeric) }

			return 0 if self.size == 0
			self.inject{ |sum, el| sum + el }.to_f / self.size
    end
  end
end