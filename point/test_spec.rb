require_relative './spec_helper'
require_relative '../main'
 
RSpec.describe Point do
  let(:file) { File.open("../fixtures/point_index.html","r") { |f| f.read }}
  let(:page) { Nokogiri::HTML.fragment(file).css(".post-wrap") }
 
  before do
    expect(Watir::Browser).to receive(:new).and_return("OK")
  end
 
  context "#get_tixtle" do
    it "gets title" do
      expect(subject.send(:get_title, page)).to eq("Чебан: Раду вступит в предвыборную кампанию, когда исчерпает админресурс")
    end
  end
 
  it "gets photo" do
    expect(subject.send(:get_photo, page)).to eq("https://i.simpalsmedia.com/point.md/news/370x220/9aa4aafd145fe5ef1320ff21b92a258b.jpg")
  end
end