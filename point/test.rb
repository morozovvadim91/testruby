require "watir"
# let(:name) {"Vadim"}

# let!(:full_name) {name + "TK"}

#дополнительно познакомиться с фишками тестов в руби

class Point
	attr_reader :browser
	attr_accessor :html :parsed_data

	def initialize
		@browser = Watir::Browser.new :chrome
		browser.goto("point.md")
		@html = ""
		@parsed_data = {}
		sleep(1)
		collect_news
	end

	def click_news()
		browser.text_field(name: "").click if field_not_found
		html = Nokogiri::HTML.fragment(browser.div(id: "post").html)
		pp html
		collect_news
	end

	def field_not_found
		browser.text.match /Какое то сожаление/
	end

	def collect_news
		click_news
		return {} unless field_not_found
		{
			title: parse_title,
			photo: parse_photo,
			description: parse_description,
			author: parse_author,
			date: parse_date,
			comments: parse_comments 
		}
	end

	def parse_title
		@parse_data[:title] = html.css(".id").text
	end

	def parse_photo
		browser
	end

	def parse_description
		browser
	end

	def parse_author
		browser
	end

	def parse_date
		browser
	end

	def parse_comments
		browser
	end
end

point = Point.new