class Country
	attr_reader :name, :currency

	def initialize(name, currency)
		@name = name 
		@currency = currency
	end
end